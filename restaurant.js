import { Form } from "./form.js";

const  employeesArr = [
    {
        id: 'dc932c20-716a-418c-8c44-ba36e158e688',
        name: 'Денис',
        surname: 'Хрущ',
        department: 'kitchen',
        position: 'cook',
        salary: 1010,
        status: 'dismissed',
        supervisor: true
    },
    {
        id: 'b09d4b69-cc00-421d-b26f-203780630f06',
        name: 'Даша',
        surname: 'Хрущ',
        department: 'hall',
        position: 'waiter',
        salary: 1010,
        status: 'works',
        supervisor: true
    },
    {
        id: '0ec6d3dc-c704-4e30-a31f-faf9bab44b11',
        name: 'Света',
        surname: 'Хрущ',
        department: 'bar',
        position: 'barmen',
        salary: 3000,
        status: 'works',
        supervisor: true
    },
    {
        id: 'a9b9c302-6b05-4ce6-9bef-d0a59bce3224',
        name: 'Вишня',
        surname: 'Хрущ',
        department: 'kitchen',
        position: 'cook',
        salary: 1010,
        status: 'works',
        supervisor: false
    },
    {
        id: 'b5d57dab-82ec-441a-99ec-6a5d5f2ac0cc',
        name: 'Виталик',
        surname: 'Хрущ',
        department: 'kitchen',
        position: 'cook',
        salary: 2000,
        status: 'works',
        supervisor: false
    },
    {
        id: 'c6009459-4348-4f76-be03-e98f4ede6ce0',
        name: 'Федя',
        surname: 'Хрущ',
        department: 'kitchen',
        position: 'cook',
        salary: 1010,
        status: 'works',
        supervisor: false
    },
    {
        id: 'be484dc0-b426-4dfa-914f-457f2e6d8c75',
        name: 'Марина',
        surname: 'Хрущ',
        department: 'bar',
        position: 'barmen',
        salary: 2000,
        status: 'dismissed',
        supervisor: false
    }
];

class Restaurant {
    constructor(employee) {
        this.name = employee.name;
        this.surname = employee.surname;
        this.department = employee.department;
        this.position = employee.position;
        this.salary = employee.salary;
        this.status = employee.status;
        this.supervisor = employee.supervisor;
        let employeeItems = JSON.parse(localStorage.getItem('employee'));
        this.employee = employeeItems !== null ? employeeItems : employeesArr;
        this.init(this.employee);
        this.formObj = new Form();
        this.formObj.createForm();
        this.bindSaveEmployee = this.saveEmployee.bind(this);
        let saveNewEmployeeBtn = document.getElementById('btn-save');
        saveNewEmployeeBtn.addEventListener('click', this.bindSaveEmployee);
    }

    sumSalary(callback) {
        let result = 0;

        for(let i = 0; i < this.employee.length; i++) {
            if (callback(this.employee[i].department)) {
                result += this.employee[i].salary;
            }
        }
        return result;
    }

    middleSalary(callback) {
        let countEmployee = 0;

        for (let i = 0; i < this.employee.length; i++) {
            if (callback(this.employee[i].department)) {
                countEmployee++;
            }
        }

        return this.sumSalary(callback) / countEmployee;
    }

    minMaxSalary(callback) {
        let employee = this.employee.filter(worker => {
            return callback(worker.department);
        });
        employee.sort(function (prev, next) {
            return prev.salary - next.salary;
        });

        return {
            min: employee[0].salary,
            max: employee[employee.length - 1].salary
        };
    }

    countEmployee(callback) {
        let employee =  this.employee.filter(person => {
            return callback(person.status);
        });

        return employee.length;
    }

    findDepartmentOfEmployee(callback) {
        let employeeArr =  this.employee.filter(person => {
            return callback(person.supervisor);
        });

        return employeeArr;
    }

    init() {
        this.createCartHtml(this.employee);
        localStorage.setItem('employee', JSON.stringify(this.employee));
    }

    createCartHtml() {
        let cartContainer = document.getElementById('cart-container');
        this.employee.forEach((itemEmployee) => {
            let cartEmployee = document.createElement('div');
            cartEmployee.className = 'cart__employee';
            let listEmployee = document.createElement('ul');
            listEmployee.className = 'list__employee';

            let cartEvent = document.createElement('div');
            cartEvent.className = 'cart__event';

            let btnEdit = document.createElement('button');
            btnEdit.id = 'btn-edit';
            btnEdit.className = 'btn-edit';
            btnEdit.innerText = 'Edit';
            btnEdit.setAttribute('data-employee-id', itemEmployee.id);
            btnEdit.addEventListener('click', this.editCart.bind(this));

            let btnDelete = document.createElement('button');
            btnDelete.id = 'btn-delete';
            btnDelete.className = 'btn-delete';
            btnDelete.innerText = 'Delete';
            btnDelete.setAttribute('data-employee-id', itemEmployee.id);
            btnDelete.addEventListener('click', this.deleteCart.bind(this));

            cartEvent.appendChild(btnEdit);
            cartEvent.appendChild(btnDelete);

            for (let item in itemEmployee) {
                if (item !== 'id') {
                    let fieldEmployee = document.createElement('li');
                    fieldEmployee.innerText = item + ': ' + itemEmployee[item];
                    listEmployee.appendChild(fieldEmployee);
                }
            }

            cartEmployee.appendChild(listEmployee);
            cartEmployee.appendChild(cartEvent);
            cartContainer.appendChild(cartEmployee);
        })
    }

    editCart(event) {
        let employeeId = event.currentTarget.getAttribute('data-employee-id');
        let employees = JSON.parse(localStorage.getItem('employee'));

        this.employee = employees !== null ? employees : [];
        let editEmployee = this.employee.filter((person) => {
            if (person.id === employeeId) {
                return true;
            }
        }).shift();

        let inputName = document.getElementById('name');
        inputName.value = editEmployee.name;
        let inputSurname = document.getElementById('surname');
        inputSurname.value = editEmployee.surname;
        let inputSalary = document.getElementById('salary');
        inputSalary.value = editEmployee.salary;
        let inputDepartment = document.getElementById('department');
        inputDepartment.value = editEmployee.department;
        let inputPosition = document.getElementById('position');
        inputPosition.value = editEmployee.position;
        let inputStatus = document.getElementById('status');
        inputStatus.value = editEmployee.status;
        let inputSupervisor = document.getElementById('supervisor');
        inputSupervisor.value = editEmployee.supervisor;

        let saveBtn = document.getElementById('btn-save');
        saveBtn.setAttribute('data-employee-id', editEmployee.id);
        saveBtn.addEventListener('click', this.saveChangedEmployee.bind(this));
        saveBtn.removeEventListener('click', this.bindSaveEmployee);
    }

    //сохраняет изменного сотрудника в карточку
    saveChangedEmployee(event) {
        let employeeId = event.currentTarget.getAttribute('data-employee-id');
        let employees = JSON.parse(localStorage.getItem('employee'));

        let form = document.querySelector('form');
        let formData = new FormData(form);

        this.employee = employees.map((person) => {
            if (person.id === employeeId) {
                person.name = formData.get('name');
                person.surname = formData.get('surname');
                person.salary = formData.get('salary');
                person.department = formData.get('department');
                person.position = formData.get('position');
                person.status = formData.get('status');
                person.supervisor = formData.get('supervisor');
            }
            return person;
        });

        localStorage.setItem('employee', JSON.stringify(this.employee));
        this.clearCartsBlock();
        this.init();
    }

    clearCartsBlock() {
        let cartsBlock = document.getElementById('cart-container');
        cartsBlock.innerText = '';
    }

    deleteCart(event) {
        let employeeId = event.currentTarget.getAttribute('data-employee-id');
        let employees = JSON.parse(localStorage.getItem('employee'));

        this.employee = employees.filter((person) => {
            if (person.id !== employeeId) {
                return true;
            }
        });

        localStorage.setItem('employee', JSON.stringify(this.employee));
        this.clearCartsBlock();
        this.init();
    }

    //сохраняет нового сотрудника в карточку
    saveEmployee(event) {
        event.preventDefault();
        let form = document.querySelector('form');
        let formData = new FormData(form);

        this.employee = JSON.parse(localStorage.getItem('employee'));

        let newEmployee = {
            id: crypto.randomUUID(),
            name: formData.get('name'),
            surname: formData.get('surname'),
            department: formData.get('department'),
            position: formData.get('position'),
            salary: formData.get('salary'),
            status: formData.get('status'),
            supervisor: formData.get('supervisor')
        };

        form.reset();

        this.employee.push(newEmployee);
        this.clearCartsBlock();
        this.init();
    }
}
let restaurant = new Restaurant(employeesArr);

restaurant.sumSalary((value) => value === 'kitchen');
restaurant.middleSalary((value) => value === 'kitchen');
restaurant.minMaxSalary((value) => value === 'kitchen');
restaurant.countEmployee((value) => value === 'dismissed');
restaurant.findDepartmentOfEmployee((value) => value === false);

document.addEventListener("DOMContentLoaded", function(event) {
});
