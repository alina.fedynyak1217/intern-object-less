export class Form {
    createForm() {
        let form = document.getElementById('form-employee');
        if (form.length !== undefined) {
            return;
        }

        let formCart = document.createElement('form');
        formCart.className = 'cart-form';
        formCart.id = 'cart-form';
        let formTitle = document.createElement('h1');
        formTitle.innerText = 'Form Add';
        let blockName = document.createElement('div');
        let nameLabel = document.createElement('label');
        nameLabel.for = 'name';
        nameLabel.innerText = 'Name:';
        let nameInput = document.createElement('input');
        nameInput.type = 'text';
        nameInput.name = 'name';
        nameInput.id = 'name';
        nameInput.value = '';

        blockName.appendChild(nameLabel);
        blockName.appendChild(nameInput);

        formCart.appendChild(formTitle);
        formCart.appendChild(blockName);

        let blockSurname = document.createElement('div');
        let surnameLabel = document.createElement('label');
        surnameLabel.for = 'surname';
        surnameLabel.innerText = 'Surname:';
        let surnameInput = document.createElement('input');
        surnameInput.type = 'text';
        surnameInput.name = 'surname';
        surnameInput.id = 'surname';
        surnameInput.value = '';

        blockSurname.appendChild(surnameLabel);
        blockSurname.appendChild(surnameInput);
        formCart.appendChild(blockSurname);

        let blockSalary = document.createElement('div');
        let salaryLabel = document.createElement('label');
        salaryLabel.for = 'salary';
        salaryLabel.innerText = 'Salary:';
        let salaryInput = document.createElement('input');
        salaryInput.type = 'text';
        salaryInput.name = 'salary';
        salaryInput.id = 'salary';
        salaryInput.value = '';

        blockSalary.appendChild(salaryLabel);
        blockSalary.appendChild(salaryInput);
        formCart.appendChild(blockSalary);

        let blockDepartment = document.createElement('div');
        let departmentLabel = document.createElement('label');
        departmentLabel.for = 'department';
        departmentLabel.innerText = 'Department:';
        let departmentSelect = document.createElement('select');
        departmentSelect.id = 'department';
        departmentSelect.name = 'department';
        let departmentOptionBar = document.createElement('option');
        departmentOptionBar.value = 'bar';
        departmentOptionBar.innerText = 'bar';
        let departmentOptionKitchen = document.createElement('option');
        departmentOptionKitchen.value = 'kitchen';
        departmentOptionKitchen.innerText = 'kitchen';
        let departmentOptionHall = document.createElement('option');
        departmentOptionHall.value = 'hall';
        departmentOptionHall.innerText = 'hall';

        blockDepartment.appendChild(departmentLabel);
        departmentSelect.appendChild(departmentOptionBar);
        departmentSelect.appendChild(departmentOptionKitchen);
        departmentSelect.appendChild(departmentOptionHall);
        blockDepartment.appendChild(departmentSelect);
        formCart.appendChild(blockDepartment);

        let blockPosition = document.createElement('div');
        let positionLabel = document.createElement('label');
        positionLabel.for = 'position';
        positionLabel.innerText = 'Position:';
        let positionSelect = document.createElement('select');
        positionSelect.id = 'position';
        positionSelect.name = 'position';
        let positionOptionBarmen = document.createElement('option');
        positionOptionBarmen.value = 'barmen';
        positionOptionBarmen.innerText = 'barmen';
        let positionOptionCook = document.createElement('option');
        positionOptionCook.value = 'cook';
        positionOptionCook.innerText = 'cook';
        let positionOptionWaite = document.createElement('option');
        positionOptionWaite.value = 'waite';
        positionOptionWaite.innerText = 'waite';

        blockPosition.appendChild(positionLabel);
        positionSelect.appendChild(positionOptionBarmen);
        positionSelect.appendChild(positionOptionCook);
        positionSelect.appendChild(positionOptionWaite);
        blockPosition.appendChild(positionSelect);
        formCart.appendChild(blockPosition);

        let blockStatus = document.createElement('div');
        let statusLabel = document.createElement('label');
        statusLabel.for = 'status';
        statusLabel.innerText = 'Status:';
        let statusSelect = document.createElement('select');
        statusSelect.id = 'status';
        statusSelect.name = 'status';
        let statusOptionDismissed = document.createElement('option');
        statusOptionDismissed.value = 'dismissed';
        statusOptionDismissed.innerText = 'dismissed';
        let statusOptionWorks = document.createElement('option');
        statusOptionWorks.value = 'works';
        statusOptionWorks.innerText = 'works';

        blockStatus.appendChild(statusLabel);
        statusSelect.appendChild(statusOptionDismissed);
        statusSelect.appendChild(statusOptionWorks);
        blockStatus.appendChild(statusSelect);
        formCart.appendChild(blockStatus);

        let blockCheckbox = document.createElement('div');
        blockCheckbox.className = 'checkbox';
        let checkboxLabel = document.createElement('label');
        checkboxLabel.for = 'supervisor';
        checkboxLabel.innerText = 'Supervisor:';
        let checkboxInput = document.createElement('input');
        checkboxInput.type = 'checkbox';
        checkboxInput.name = 'supervisor';
        checkboxInput.id = 'supervisor';

        blockCheckbox.appendChild(checkboxLabel);
        blockCheckbox.appendChild(checkboxInput);
        formCart.appendChild(blockCheckbox);

        let blockBtn = document.createElement('div');
        let buttonAdd = document.createElement('button');
        buttonAdd.className = 'add';
        buttonAdd.id = 'btn-save';
        buttonAdd.innerText = 'Add';

        blockBtn.appendChild(buttonAdd);
        formCart.appendChild(blockBtn);
        form.appendChild(formCart);
    }
}